#include <stdio.h>
#include "testCommon.h"

void testStart(char *mes) {
	fprintf(stderr, "%s ... ", mes);
}

void testEnd() {
	message("Ok\n");
}

void assert_equals_int_func(int a, int b, char *fname, int line) {
	if (a != b)
		messend4("Error in %s(%d): a != b (%d != %d)\n", fname, line, a, b);
}

void assert_not_equals_int_func(int a, int b, char *fname, int line) {
	if (a == b)
		message4("Error in %s(%d): a == b (%d == %d)\n", fname, line, a, b);
}

void assert_equals_float_func(float a, float b, char *fname, int line) {
	if (isnan(a) || isnan(b))
		messend4("Error in %s(%d): a(%f) or b(%f) is NaN\n", fname, line, a, b);
	if (fabs(a - b) > DELTA)
		message4("Error in %s(%d): a != b (%f != %f)\n", fname, line, a, b);
}

void assert_equals_double_func(double a, double b, char *fname, int line) {
	if (isnan(a) || isnan(b))
		messend4("Error in %s(%d): a(%f) or b(%f) is NaN\n", fname, line, a, b);
	if (fabs(a - b) > DELTA)
		messend4("Error in %s(%d): a != b (%f != %f)\n", fname, line, a, b);
}
